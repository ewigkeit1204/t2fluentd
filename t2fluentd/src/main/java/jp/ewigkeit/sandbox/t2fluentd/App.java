/*
 * Copyright 2013 Keisuke.K
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.ewigkeit.sandbox.t2fluentd;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.fluentd.logger.FluentLogger;

import twitter4j.Status;
import twitter4j.StatusAdapter;
import twitter4j.StatusListener;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.UserStreamAdapter;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

/**
 * @author Keisuke.K
 */
public class App {

	static final String TAG = "t2fluentd";
	static final String LABEL = "stream";
	static final String TOKEN_STORE = ".t2fluentd";
	static final String KEY_TOKEN = "token";
	static final String KEY_TOKEN_SECRET = "tokenSecret";

	static final FluentLogger LOGGER = FluentLogger.getLogger(TAG);

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Twitter twitter = TwitterFactory.getSingleton();
		AccessToken accessToken = loadAccessToken();

		if (accessToken == null) {
			accessToken = getAccessToken(twitter);
			if (accessToken == null) {
				return;
			}
		}

		twitter.setOAuthAccessToken(accessToken);

		StatusListener listener = new StatusAdapter() {
			@Override
			public void onStatus(Status status) {
				Map<String, Object> data = new HashMap<String, Object>();
				data.put("name", status.getUser().getName());
				data.put("screenName", status.getUser().getScreenName());
				data.put("text", status.getText());

				LOGGER.log(LABEL, data);
			}
		};

		TwitterStream twitterStream = new TwitterStreamFactory().getInstance(accessToken);
		twitterStream.addListener(listener);
		twitterStream.addListener(new UserStreamAdapter());
		twitterStream.user();
	}

	static AccessToken loadAccessToken() {
		Properties tokenProp = new Properties();
		String token = null;
		String tokenSecret = null;

		try (Reader in = new BufferedReader(new FileReader(TOKEN_STORE))) {
			tokenProp.load(in);
			token = tokenProp.getProperty(KEY_TOKEN);
			tokenSecret = tokenProp.getProperty(KEY_TOKEN_SECRET);
		} catch (IOException e) {
			// System.err.println(e.getMessage());
			return null;
		}

		return new AccessToken(token, tokenSecret);
	}

	static AccessToken getAccessToken(Twitter twitter) {
		RequestToken requestToken;

		try {
			requestToken = twitter.getOAuthRequestToken();
		} catch (TwitterException e) {
			System.err.println(e.getMessage());
			return null;
		}

		String authURL = requestToken.getAuthorizationURL();
		System.console().printf("Get PIN %s\n", authURL);

		AccessToken accessToken = null;
		while (accessToken == null) {
			String pin = System.console().readLine("PIN: ");
			try {
				accessToken = twitter.getOAuthAccessToken(requestToken, pin);
			} catch (TwitterException e) {
				System.err.println(e.getMessage());
			}
		}

		storeAccessToken(accessToken);
		return accessToken;
	}

	static void storeAccessToken(AccessToken accessToken) {
		Properties tokenProp = new Properties();
		tokenProp.setProperty(KEY_TOKEN, accessToken.getToken());
		tokenProp.setProperty(KEY_TOKEN_SECRET, accessToken.getTokenSecret());

		try (Writer out = new BufferedWriter(new FileWriter(TOKEN_STORE))) {
			tokenProp.store(out, null);
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

}
